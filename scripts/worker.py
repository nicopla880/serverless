import os
from importlib import import_module

from flask import Flask, jsonify, request

app = Flask(__name__)


@app.route("/")
def home():
    return jsonify({'mesagge': "Hello, World!"})


@app.route("/endpoints/")
def endpoint():
    app_path = '/app/app/'
    excludes = ['__init__.py', '__pycache__']
    endpoint_list = []
    for package in os.listdir(app_path):
        if package not in excludes:
            for files in os.listdir(app_path + package):
                if files not in excludes:
                    endpoint_list.append(
                        '/api/{0}/{1}'.format(
                            package, files.replace('.py', '')
                        )
                    )
    return jsonify({'endpoint': endpoint_list})


@app.route("/api/<folder>/<file>/")
def api(folder, file):
    module_executed = import_module('app.{0}.{1}'.format(folder, file))
    context = {
        'method': request.method,
        'request': request
    }
    if request.method == 'GET':
        args = request.args
    elif request.method == 'POST':
        args = request.form
    else:
        args = {}
    return jsonify(module_executed.handler(context, args))


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
