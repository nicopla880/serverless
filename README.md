# Serverless Docker

Docker container that run serverless script.

## Dockerfile

### Build

```shell
$ docker build . -t serverless:0.1
```

### Run
```shell
$ docker run -p 5000:5000 -v $(pwd)/app:/app/app serverless:0.1
```

## docker-compose

### Build
```shell
$ docker-compose build
```

### Run
```shell
$ docker-compose up -d
```

### Down
```shell
$ docker-compose down
```

## Example api_file.py
```python
def handler(context, args):
    return {'result': 'OK', 'message': ''}
```
